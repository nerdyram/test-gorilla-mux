package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"test-gorilla-mux/userservice"
)

func main() {
	r := RegisterRoutes()
	http.ListenAndServe(":8000", r)
}

func RegisterRoutes() *mux.Router {
	m := userservice.GenerateRouteHandlerMap()
	r := mux.NewRouter()
	for k, v := range m {
		r.HandleFunc(k, v)
	}
	return r
}
