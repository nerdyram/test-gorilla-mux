package userservice

import (
	"net/http"
)

func GetUserHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Got a user"))
}

func GenerateRouteHandlerMap() map[string]func(w http.ResponseWriter, r *http.Request) {
	routeMap := map[string]func(w http.ResponseWriter, r *http.Request){
		"/api/routes/getUser": GetUserHandler,
	}
	return routeMap
}
